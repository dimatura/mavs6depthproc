
from fosforo.parafina.op.geom_tf import FlipLR
from fosforo.parafina.op.misc import ScaleOffset
from fosforo.parafina.op.cnnpreproc import HwcToNchw
# from fosforo.parafina.op.depth import DisparityToDepth
from fosforo.parafina.op.photo_tf import RectangleMask
from fosforo.parafina.op.photo_tf import BlobMask
from fosforo.parafina.op.misc import Concatenate
from fosforo.parafina.pipeline import LineSection


def build_train_ops(gray_key, disp_key, x_key, y_key, scale):
    ops = LineSection()

    ops['rmask1'] = RectangleMask(mapping={disp_key: disp_key},
                                  prob=0.2,
                                  max_scale=0.3)

    ops['rmask2'] = RectangleMask(mapping={disp_key: disp_key},
                                  prob=0.1,
                                  max_scale=0.5)

    ops['bmask'] = BlobMask(mapping={disp_key: disp_key},
                            prob=0.1,
                            max_scale=0.5,
                            fraction=0.2)

    ops['fliplr'] = FlipLR(mapping={gray_key: gray_key,
                                    disp_key: disp_key,
                                    y_key: y_key})

    ops['scaledisp'] = ScaleOffset(mapping={disp_key: disp_key,
                                            y_key: y_key},
                                            scale=scale,
                                            offset=0.)

    ops['scalegray'] = ScaleOffset(mapping={gray_key: gray_key},
                                   scale=1./255.,
                                   offset=-0.5)

    ops['cat'] = Concatenate(mapping={(gray_key, disp_key): x_key})

    ops['cnnpp'] = HwcToNchw(mapping={x_key: x_key, y_key: y_key})
    return ops


def build_valid_ops(gray_key, disp_key, x_key, y_key, scale):
    ops = LineSection()
    ops['scaledisp'] = ScaleOffset(mapping={disp_key: disp_key,
                                            y_key: y_key},
                                            scale=scale,
                                            offset=0.)

    ops['scalegray'] = ScaleOffset(mapping={gray_key: gray_key},
                                   scale=1./255.,
                                   offset=-0.5)



    ops['cat'] = Concatenate(mapping={(gray_key, disp_key): x_key})

    ops['cnnpp'] = HwcToNchw(mapping={x_key: x_key, y_key: y_key})

    return ops
