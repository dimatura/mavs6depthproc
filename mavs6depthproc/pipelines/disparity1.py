
from fosforo.parafina.op.geom_tf import FlipLR
from fosforo.parafina.op.misc import ScaleOffset
from fosforo.parafina.op.cnnpreproc import HwcToNchw
# from fosforo.parafina.op.depth import DisparityToDepth
from fosforo.parafina.op.photo_tf import RectangleMask
from fosforo.parafina.op.photo_tf import BlobMask
from fosforo.parafina.pipeline import LineSection


def build_train_ops(x_key, y_key, scale):
    ops = LineSection()

    ops['rmask1'] = RectangleMask(mapping={x_key: x_key},
                                  prob=0.2,
                                  max_scale=0.3)

    ops['rmask2'] = RectangleMask(mapping={x_key: x_key},
                                  prob=0.1,
                                  max_scale=0.5)

    ops['bmask'] = BlobMask(mapping={x_key: x_key},
                            prob=0.1,
                            max_scale=0.5,
                            fraction=0.2)

    ops['fliplr'] = FlipLR(mapping={x_key: x_key,
                                    y_key: y_key})
    ops['scale'] = ScaleOffset(mapping={x_key: x_key,
                                        y_key: y_key},
                               scale=scale,
                               offset=0.)

    ops['cnnpp'] = HwcToNchw(mapping={x_key: x_key,
                                      y_key: y_key})

    return ops


def build_valid_ops(x_key, y_key, scale):
    ops = LineSection()
    ops['scale'] = ScaleOffset(mapping={x_key: x_key,
                                        y_key: y_key},
                               scale=scale,
                               offset=0.)

    ops['cnnpp'] = HwcToNchw(mapping={x_key: x_key,
                                      y_key: y_key})

    return ops
