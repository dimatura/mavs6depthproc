from collections import OrderedDict
import cv2

from fosforo.data import DirDataset


class DisparityDataset(DirDataset):
    def __init__(self,
                 base_dir,
                 instance_list_fname,
                 split_key,
                 pipeline,
                 x_fname='nerian_disp16.png',
                 y_fname='colmap_disp16.png',
                 x_key='nerian_disp',
                 y_key='colmap_disp'):
        super(DisparityDataset, self).__init__(base_dir,
                                               instance_list_fname,
                                               split_key,
                                               pipeline)
        self.x_fname = x_fname
        self.y_fname = y_fname
        self.x_key = x_key
        self.y_key = y_key

    def _init_blob(self, idir):
        blob = OrderedDict()
        blob['x_fname'] = self.base_dir/idir/self.x_fname
        blob['y_fname'] = self.base_dir/idir/self.y_fname

        indisp = cv2.imread(blob['x_fname'], cv2.IMREAD_UNCHANGED)
        outdisp = cv2.imread(blob['y_fname'], cv2.IMREAD_UNCHANGED)
        # TODO the 256 constant is hard-coded from
        # variable used to encode original float disparities from
        # nerian to a uint16 (from a script somewhere)
        indisp = (indisp.astype('f4')/256.)
        outdisp = (outdisp.astype('f4')/256.)

        blob[self.x_key] = indisp
        blob[self.y_key] = outdisp
        return blob


class RGBDDataset(DirDataset):
    def __init__(self,
                 base_dir,
                 instance_list_fname,
                 split_key,
                 pipeline,
                 dispx_fname='nerian_disp16.png',
                 grayx_fname='rgb16.jpg',
                 y_fname='colmap_disp16.png',
                 dispx_key='nerian_disp',
                 grayx_key='nerian_gray',
                 y_key='colmap_disp'):
        super(RGBDDataset, self).__init__(base_dir,
                                               instance_list_fname,
                                               split_key,
                                               pipeline)
        self.dispx_fname = dispx_fname
        self.grayx_fname = grayx_fname
        self.y_fname = y_fname
        self.grayx_key = grayx_key
        self.dispx_key = dispx_key
        self.y_key = y_key

    def _init_blob(self, idir):
        blob = OrderedDict()
        blob['dispx_fname'] = self.base_dir/idir/self.dispx_fname
        blob['grayx_fname'] = self.base_dir/idir/self.grayx_fname
        blob['y_fname'] = self.base_dir/idir/self.y_fname

        indisp = cv2.imread(blob['dispx_fname'], cv2.IMREAD_UNCHANGED)
        ingray = cv2.imread(blob['grayx_fname'], cv2.IMREAD_GRAYSCALE)
        outdisp = cv2.imread(blob['y_fname'], cv2.IMREAD_UNCHANGED)
        # TODO the 256 constant is hard-coded from
        # variable used to encode original float disparities from
        # nerian to a uint16 (from a script somewhere)
        indisp = (indisp.astype('f4')/256.)
        ingray = ingray.astype('f4')
        outdisp = (outdisp.astype('f4')/256.)

        blob[self.dispx_key] = indisp
        blob[self.grayx_key] = ingray
        blob[self.y_key] = outdisp
        return blob
