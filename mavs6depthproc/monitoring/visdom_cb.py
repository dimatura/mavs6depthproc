from __future__ import print_function

import logging

import numpy as np

from fosforo.utils import viz_utils

from fosforo.monitoring.visdom_cb import VisdomBaseCb


log = logging.getLogger(__name__)


class VisdomDisparityCb(VisdomBaseCb):
    def __init__(self,
                 name,
                 vmin=None,
                 vmax=None,
                 cmap='jet',
                 preproc=None):
        self.name = name
        self.vmin = vmin
        self.vmax = vmax
        self.cmap = cmap
        self.preproc = preproc

    def init(self, app):
        super(VisdomDisparityCb, self).init(app)

    def batch(self, app, batch, **kwargs):
        if not self.vis.check_connection():
            log.warn('no vis connection')
            return

        xdisp = batch['nerian_disp']
        ydisp = batch['colmap_disp']
        yhat = kwargs['yhat']

        rnd_state = np.random.RandomState(app.state['itr_ctr'])
        r_ix = rnd_state.randint(len(xdisp))

        xdisp = xdisp[r_ix, :].data.cpu().numpy().squeeze()
        ydisp = ydisp[r_ix, :].data.cpu().numpy().squeeze()
        yhat = yhat[r_ix, :].data.cpu().numpy().squeeze()

        if self.preproc is not None:
            xdisp = self.preproc(xdisp)
            ydisp = self.preproc(ydisp)
            yhat = self.preproc(yhat)

        xviz = viz_utils.colorize_scalar(xdisp, vmin=self.vmin, vmax=self.vmax, cmap=self.cmap, to_c01=True)
        yviz = viz_utils.colorize_scalar(ydisp, vmin=self.vmin, vmax=self.vmax, cmap=self.cmap, to_c01=True)
        yhatviz = viz_utils.colorize_scalar(yhat, vmin=self.vmin, vmax=self.vmax, cmap=self.cmap, to_c01=True)

        viz = np.concatenate((xviz[None, ...], yviz[None, ...], yhatviz[None, ...]), 0)

        self.vis.images(viz, win=self.name, opts={'title': self.name})
