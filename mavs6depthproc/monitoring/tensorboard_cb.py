from __future__ import print_function

import logging
from collections import defaultdict

import numpy as np
from PIL import Image

import torch
import torch.nn as nn
import torch.nn.functional as F

from fosforo.utils import viz_utils

from fosforo.monitoring.tensorboard_cb import TensorboardBaseCb

import imgutils


log = logging.getLogger(__name__)


class TensorboardDisparityCb(TensorboardBaseCb):
    def __init__(self,
                 name,
                 vmin=None,
                 vmax=None,
                 cmap='jet',
                 preproc=None,
                 yhat_is_depth=False):
        self.name = name
        self.vmin = vmin
        self.vmax = vmax
        self.cmap = cmap
        self.preproc = preproc
        self.yhat_is_depth = yhat_is_depth

    def init(self, app):
        super(TensorboardDisparityCb, self).init(app)

    def batch(self, app, batch, **kwargs):

        xdisp = batch['nerian_disp']
        ydisp = batch['colmap_disp']
        yhat = kwargs['yhat']

        rnd_state = np.random.RandomState(app.state['itr_ctr'])
        r_ix = rnd_state.randint(len(xdisp))

        xdisp = xdisp[r_ix, :].data.cpu().numpy().squeeze()
        ydisp = ydisp[r_ix, :].data.cpu().numpy().squeeze()
        yhat = yhat[r_ix, :].data.cpu().numpy().squeeze()

        if self.preproc is not None:
            xdisp = self.preproc(xdisp)
            ydisp = self.preproc(ydisp)
            if self.yhat_is_depth:
                # TODO unhardcode
                yhat = 178.443/yhat.clip(1., 400.)
            else:
                yhat = self.preproc(yhat)

        xviz = viz_utils.colorize_scalar(xdisp, vmin=self.vmin, vmax=self.vmax, cmap=self.cmap, to_c01=False)
        yviz = viz_utils.colorize_scalar(ydisp, vmin=self.vmin, vmax=self.vmax, cmap=self.cmap, to_c01=False)
        yhatviz = viz_utils.colorize_scalar(yhat, vmin=self.vmin, vmax=self.vmax, cmap=self.cmap, to_c01=False)

        #viz = np.concatenate((xviz[None, ...], yviz[None, ...], yhatviz[None, ...]), 0)

        grid = imgutils.montage((Image.fromarray(xviz),
                                 Image.fromarray(yviz),
                                 Image.fromarray(yhatviz)),
                                1, 3)
        grid = viz_utils.img_01c_to_c01(np.asarray(grid))
        itr_ctr = app.state['itr_ctr']
        self.summary_writer.add_image(self.name, grid, itr_ctr)


class TensorboardDispDepthMetricsCb(TensorboardBaseCb):
    def __init__(self, name, disp_norm_scale=1., Tx=-1., yhat_is_depth=False):
        self.name = name
        self.Tx = Tx
        self.disp_norm_scale = disp_norm_scale
        self.yhat_is_depth = yhat_is_depth

    def init(self, app):
        super(TensorboardBaseCb, self).init(app)

    def epoch_start(self, app):
        self.itr_ctr = app.state['itr_ctr']
        self.entries = []

    def _l1_disp_err(self, y, yhat):
        """ weigh all pixels equally """
        nz_mask = (y != 0.)
        nnz = nz_mask.sum()
        if nnz == 0:
            return 0.
        y2 = (1./self.disp_norm_scale)*y

        if self.yhat_is_depth:
            # clamp to avoid numerical issues -- TODO same for y
            yhat2 = yhat.clamp(1., 400.)
            # convert to disp -- no scaling
            yhat2 = -self.Tx/yhat2
        else:
            # just apply scaling
            yhat2 = (1./self.disp_norm_scale)*yhat
        l1 = (yhat2[nz_mask] - y2[nz_mask]).abs().mean()
        return l1.item()

    def _l1_depth_err(self, y, yhat):
        """ weigh all pixels equally """
        nz_mask = (y != 0.)
        nnz = nz_mask.sum()
        if nnz == 0:
            return 0.
        # scale y
        y_nz = y[nz_mask]*(1./self.disp_norm_scale)

        if self.yhat_is_depth:
            # convert both to depth, clamp
            yhat_nz = yhat[nz_mask].clamp(1., 400.)
            y_nz = ((-self.Tx)/y_nz).clamp(1., 400.)
            # divide by nnz first, helps with inf
            l1 = (yhat_nz/nnz.float() - y_nz/nnz.float()).abs().sum()
        else:
            # scale, then clamp
            ## clamp predicted on half a pixel (356 m on nerian).
            yhat_nz = yhat[nz_mask]*(1./self.disp_norm_scale)
            yhat_nz = yhat_nz.clamp(min=0.5)
            # for fairness, clamp y as well
            y_nz = y_nz.clamp(min=0.5)

            # more numerically stable than naive way I believe
            delta = (yhat_nz - y_nz).abs()
            disp_prod = (yhat_nz * y_nz).abs()
            l1 = ((abs(self.Tx)/disp_prod)*delta).mean()
        return l1.item()

    def batch(self, app, batch, **kwargs):
        # use .item()?
        with torch.no_grad():
            y = batch['colmap_disp'].cpu()
            yhat = kwargs['yhat'].cpu()

            nnz = (y != 0.).sum().item()
            if nnz == 0:
                l1_disp = 0.
                l2_depth = 0.
            else:
                l1_disp = self._l1_disp_err(y, yhat)
                l1_depth = self._l1_depth_err(y, yhat)

            self.entries.append({'nnz': nnz,
                                 'l1_disp': l1_disp,
                                 'l1_depth': l1_depth})


    def epoch_end(self, app):
        # list of dicts to dict of lists
        entriesT = defaultdict(list)
        for entry in self.entries:
            for k,v in entry.iteritems():
                entriesT[k].append(v)

        for k,v in entriesT.iteritems():
            entriesT[k] = np.asarray(v)

        N = entriesT['nnz'].sum()

        if N == 0:
            log.warn('no non-zero batches in validation set')
            return

        ratios = entriesT['nnz']/float(N)

        for k, means in entriesT.iteritems():
            if k == 'nnz':
                continue
            mean = (ratios*means).sum()

            log.info('valid mean %s: %f', k, mean)
            self.summary_writer.add_scalar(self.name+'/%s'%k, mean, self.itr_ctr)
