
class CamInfo(object):
    def __init__(self):
        self.fx = 0.
        self.fy = 0.
        self.cx = 0.
        self.cy = 0.
        self.Tx = 0.
        self.width = 0
        self.height = 0

    def scaled(self, scale):
        ci = CamInfo()
        ci.fx = scale*self.fx
        ci.fy = scale*self.fy
        ci.cx = scale*self.cx
        ci.cy = scale*self.cy
        ci.Tx = self.Tx
        ci.width = scale*self.width
        ci.height = scale*self.height
        return ci

    @staticmethod
    def from_camera_info(msg):
        cinfo = CamInfo()
        cinfo.fx = msg.P[0]
        cinfo.fy = msg.P[5]
        cinfo.cx = msg.P[2]
        cinfo.cy = msg.P[6]
        cinfo.Tx = msg.P[3]
        cinfo.width = msg.width
        cinfo.height = msg.height
