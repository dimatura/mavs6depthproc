
import torch
from torch import nn

from fosforo.layers import SparsityInvariantConvRelu2d as SICR
from fosforo.init import noisy_dirac_init
from fosforo.base import FosforoModel


class Net(FosforoModel):
    def __init__(self):
        super(Net, self).__init__()
        self.cbr1 = SICR(1, 16, 3, relu=True)
        self.cbr2 = SICR(16, 16, 3, relu=True)
        self.cbr3 = SICR(16, 16, 3, relu=True)
        self.cbr4 = SICR(16, 1, 3, relu=False)

    def initialize(self):
        noisy_dirac_init_(self.cbr1.conv.weight)
        noisy_dirac_init_(self.cbr2.conv.weight)
        noisy_dirac_init_(self.cbr3.conv.weight)
        noisy_dirac_init_(self.cbr4.conv.weight)
        nn.init.constant_(self.cbr1.bias, 0.)
        nn.init.constant_(self.cbr2.bias, 0.)
        nn.init.constant_(self.cbr3.bias, 0.)
        nn.init.constant_(self.cbr4.bias, 0.)
        #nn.init.kaiming_normal_(self.cbr3.conv.weight)

    def forward(self, x):
        #x = nn.functional.pad(x, (4, 4, 1, 1)
        #y = ucbr4[:, :, 1:1+30, 4:4+40]
        # get disparity since input is img+disp
        #x = x[:, 1:2]
        mask = (x != 0.0).float()
        self.x_out = torch.cat((mask, x), dim=1)
        self.cbr1_out = self.cbr1(self.x_out)
        self.cbr2_out = self.cbr2(self.cbr1_out)
        self.cbr3_out = self.cbr3(self.cbr2_out)
        self.cbr4_out = self.cbr3(self.cbr3_out)
        return self.cbr4_out[:, 1:2]
