import torch
from torch import nn
import torch.nn.functional as F

from fosforo.layers import SparsityInvariantConvRelu2d as SICR
from fosforo.layers import ResidualSICR
from fosforo.models.base import FosforoModel


class Net(FosforoModel):
    def __init__(self):
        super(Net, self).__init__()
        self.nin0 = nn.Conv2d(1, 1, 1, bias=False)
        self.cbrin = SICR(1, 16, 1)
        self.rcbr1 = ResidualSICR(16, 16, 0, 3)
        self.rcbr2 = ResidualSICR(16, 16, 0, 3)
        self.rcbr3 = ResidualSICR(16, 16, 0, 3)
        self.cbrout = SICR(16, 1, 1, relu=False)

    def initialize(self):
        nn.init.constant_(self.nin0.weight, 1.)
        noisy_dirac_init_(self.cbrin.conv.weight)
        self.rcbr1.initialize()
        self.rcbr2.initialize()
        self.rcbr3.initialize()
        nn.init.kaiming_normal_(self.cbrout.conv.weight)

    def forward(self, x):
        # note we don't use spatial dropout, to add sparsity
        # TODO probably should add a masking op in pipeline
        x = F.dropout(x, p=0.05)

        x = self.nin0(x)
        mask = (x != 0.0).float()

        mask, x = self.cbrin(mask, x)
        mask, x = self.rcbr1(mask, x)
        mask, x = self.rcbr2(mask, x)
        mask, x = self.rcbr3(mask, x)
        mask, x = self.cbrout(mask, x)
        return mask*x
