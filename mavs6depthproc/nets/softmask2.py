import torch
from torch import nn
import torch.nn.functional as F

from fosforo.layers import SparsityInvariantConvRelu2d as SICR
from fosforo.layers import ResidualSICR
from fosforo.layers import CRP, CBRP, ResConv
from fosforo.init import normal_dirac_init_
from fosforo.models.base import FosforoModel


class SoftMaskConv(nn.Module):
    def __init__(self, in_channels, mask_in_channels, out_channels, kernel_size):
        super(SoftMaskConv, self).__init__()
        padding = int(kernel_size/2)
        self.scr = nn.Conv2d(in_channels, out_channels, kernel_size, padding=padding)
        self.mcr = nn.Conv2d(mask_in_channels, out_channels, kernel_size, padding=padding)

    def initialize(self):
        normal_dirac_init_(self.scr.weight, 0.1)
        #nn.init.constant_(self.mcr.weight, 1.)
        normal_dirac_init_(self.mcr.weight, 0.1)

    def forward(self, mask, x):
        mcr = torch.sigmoid(self.mcr(mask))
        scr = F.leaky_relu(self.scr(x))
        out = mcr*scr
        return mcr, out


class Net(FosforoModel):
    def __init__(self):
        super(Net, self).__init__()
        self.nin0 = nn.Conv2d(1, 1, 1, bias=False)
        self.smc1 = SoftMaskConv(1, 1, 64, 3)
        self.smc2 = SoftMaskConv(64, 64, 64, 3)
        self.smc3 = SoftMaskConv(64, 64, 64, 3)
        self.smc4 = SoftMaskConv(64, 64, 64, 3)
        self.smcout = SoftMaskConv(64, 64, 1, 1)

    def initialize(self):
        nn.init.constant_(self.nin0.weight, 1.)
        self.smc1.initialize()
        self.smc2.initialize()
        self.smc3.initialize()
        self.smc4.initialize()
        self.smcout.initialize()

    def forward(self, x):
        # note we don't use spatial dropout, to add sparsity
        # TODO probably should add a masking op in pipeline
        x = F.dropout(x, p=0.05)
        mask = (x != 0.0).float()

        x = self.nin0(x)

        mask, x = self.smc1(mask, x)
        mask, x = self.smc2(mask, x)
        mask, x = self.smc3(mask, x)
        mask, x = self.smc4(mask, x)
        mask, x = self.smcout(mask, x)

        return x
