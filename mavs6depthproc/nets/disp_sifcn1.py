import torch
from torch import nn
import torch.nn.functional as F

from fosforo.layers import SparsityInvariantConvRelu2d as SICR
from fosforo.layers import ResidualSICR
from fosforo.layers import sparse_sum
from fosforo.models.base import FosforoModel


class SiDownBlock(nn.Module):
    def __init__(self, in_channels, out_channels1, out_channels2, kernel_size=3):
        super(SiDownBlock, self).__init__()
        padding = int(kernel_size//2)
        self.conv = SICR(in_channels, out_channels1, kernel_size)
        self.pool = nn.MaxPool2d(2, 2)
        self.nin = SICR(out_channels1, out_channels2, 1)

    def forward(self, mask, x):
        # same res branch
        mask, x = self.conv(mask, x)
        ninmask, ninx = self.nin(mask, x)

        # pool branch
        mask = F.max_pool2d(mask, 2, 2)
        x = F.max_pool2d(x, 2, 2)

        return (mask, x, ninmask, ninx)


class SiUpBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1):
        super(SiUpBlock, self).__init__()
        padding = int(kernel_size//2)
        # TODO would bilinear work?
        self.up = nn.Upsample(mode='nearest', scale_factor=2)
        # TODO use maxunpool maybe? this would require indices.
        self.maskup = nn.Upsample(mode='nearest', scale_factor=2)
        self.conv = SICR(in_channels, out_channels, kernel_size)

    def forward(self, mask1, x1, mask2, x2):
        mask1up = self.maskup(mask1)
        x1up = self.up(x1)
        mask_sum, up_sum = sparse_sum(mask1up, x1up, mask2, x2)
        maskout, out = self.conv(mask_sum, up_sum)
        return maskout, out


class Net(FosforoModel):
    def __init__(self):
        super(Net, self).__init__()
        self.nin0 = nn.Conv2d(1, 1, 1, bias=False)

        # 32, 40
        self.down1 = SiDownBlock(1, 16, 16, 3) # 16, 20
        self.down2 = SiDownBlock(16, 16, 16, 3) # 8, 10
        self.down3 = SiDownBlock(16, 16, 16, 3) # 4, 5

        self.up1 = SiUpBlock(16, 16) # 8, 10
        self.up2 = SiUpBlock(16, 16) # 16, 20
        self.up3 = SiUpBlock(16, 16) # 32, 40

        self.cout = SICR(16, 1, 1)

    def initialize(self):
        nn.init.constant_(self.nin0.weight, 1.)
        #normal_dirac_init_(self.cbrin.conv.weight)
        #self.rcbr1.initialize()
        #self.rcbr2.initialize()
        #self.rcbr3.initialize()
        #nn.init.kaiming_normal_(self.cbrout.conv.weight)

    def forward(self, x):
        xrows = x.size(2)

        # note we don't use spatial dropout, to add sparsity
        # TODO probably should add a masking op in pipeline
        x = F.dropout(x, p=0.05)

        # 30, 40 -> 32, 40.
        x = F.pad(x, (0, 0, 1, 1))

        x = self.nin0(x)
        mask = (x != 0.0).float()

        mask_down1, downx1, mask1, x1 = self.down1(mask, x)
        mask_down2, downx2, mask2, x2 = self.down2(mask_down1, downx1)
        mask_down3, downx3, mask3, x3 = self.down3(mask_down2, downx2)

        #print mask_down3.shape, downx3.shape, mask3.shape, x3.shape

        mask_up1, up1 = self.up1(mask_down3, downx3, mask3, x3)
        mask_up2, up2 = self.up2(mask_up1, up1, mask2, x2)
        mask_up3, up3 = self.up3(mask_up2, up2, mask1, x1)

        mask_out, out = self.cout(mask_up3, up3)
        # unpad
        # mask_out = mask_out[:, :,  1:1+x.size(2), :]
        out = out[:, :, 1:(1+xrows), :]
        return out

