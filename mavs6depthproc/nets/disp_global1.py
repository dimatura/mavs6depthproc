import torch
from torch import nn
import torch.nn.functional as F

from fosforo.layers import SparsityInvariantConvRelu2d as SICR
from fosforo.layers import ResidualSICR
from fosforo.models.base import FosforoModel


class Net(FosforoModel):
    def __init__(self):
        super(Net, self).__init__()
        self.nin0 = nn.Conv2d(1, 1, 1, bias=False)

        self.nin1 = SICR(3, 16, 1)
        self.nin2 = SICR(16, 16, 1)
        self.nin3 = SICR(16, 1, 1, relu=False)

    def initialize(self):
        nn.init.constant_(self.nin0.weight, 1.)
        #normal_dirac_init_(self.cbrin.conv.weight)
        #self.rcbr1.initialize()
        #self.rcbr2.initialize()
        #self.rcbr3.initialize()
        #nn.init.kaiming_normal_(self.cbrout.conv.weight)

    def forward(self, x):
        xrows = x.size(2)
        x = F.dropout(x, p=0.05)
        mask = (x != 0.).float()
        x = self.nin0(x)

        # TODO think more about aspect ratio/pixels/focal
        xpx = torch.linspace(-0.5, 0.5, 40)
        ypx = torch.linspace(-0.5, 0.5, 30)
        v, u = torch.meshgrid([ypx, xpx])
        u = u.repeat((x.size(0), 1, 1, 1)).cuda()
        v = v.repeat((x.size(0), 1, 1, 1)).cuda()
        #print v.shape, u.shape, x.shape

        x1 = torch.cat((v, u, x), dim=1)

        mask, x1 = self.nin1(mask, x1)
        mask, x1 = self.nin2(mask, x1)
        mask, x1 = self.nin3(mask, x1)
        return x1
