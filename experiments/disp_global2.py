from collections import OrderedDict
from spath import Path

exp_id = str(Path(__file__).basename().stripext())

DATA_DIR = 'disp_dataset/2018_08_23'
SPLITS = DATA_DIR + '/splits1.json'
CLASS_METADATA = ''

X_KEY = 'nerian_disp'
Y_KEY = 'colmap_disp'
YHAT_KEY = 'yhat'

TRAIN_BATCH_SIZE = 4
VALID_BATCH_SIZE = 16

VALID_EVERY_NSEC = 5*60
SAVE_EVERY_NSEC = 5*60
DISPLAY_EVERY_NSEC = 2
MAX_EPOCHS = 400000

L2_DECAY = 0.000001
#L2_DECAY = 0.0
#UPDATER_INIT_LR = 0.00001
UPDATER_INIT_LR = 0.000001
#UPDATER_INIT_LR = 0.0000001
#UPDATER_INIT_LR = 1e-9
#UPDATER_INIT_LR = 1e-11
#UPDATER_INIT_LR = 1e-12
#UPDATER_INIT_LR = 5e-13
MOMENTUM = 0.95

NORM_SCALE = 1./16
Tx = -178.443


def build_model(mode='train'):
    import torch
    from torch import nn
    import torch.nn.functional as F

    from fosforo.layers import SparsityInvariantConvRelu2d as SICR
    from fosforo.layers import SparsityInvariantConvReluPool2d as SICRP
    from fosforo.layers import sparse_sum
    from fosforo.layers import ResidualSICR
    from fosforo.init import normal_dirac_init_
    from fosforo.models.base import FosforoModel

    class Net(FosforoModel):
        def __init__(self):
            super(Net, self).__init__()
            self.nin0 = nn.Conv2d(1, 1, 1, bias=False)

            self.nin1 = nn.Conv2d(4, 16, 1)
            self.nin2 = nn.Conv2d(16, 16, 1)
            self.nin3 = nn.Conv2d(16, 1, 1)

        def initialize(self):
            nn.init.constant_(self.nin0.weight, 1.)
            #normal_dirac_init_(self.cbrin.conv.weight)
            #self.rcbr1.initialize()
            #self.rcbr2.initialize()
            #self.rcbr3.initialize()
            #nn.init.kaiming_normal_(self.cbrout.conv.weight)

        def forward(self, x):
            xrows = x.size(2)
            x = F.dropout(x, p=0.05)
            mask = (x != 0.).float()
            x = self.nin0(x)

            # TODO think more about aspect ratio/pixels/focal
            xpx = torch.linspace(-0.5, 0.5, 40)
            ypx = torch.linspace(-0.5, 0.5, 30)
            v, u = torch.meshgrid([ypx, xpx])
            u = u.repeat((x.size(0), 1, 1, 1)).cuda()
            v = v.repeat((x.size(0), 1, 1, 1)).cuda()
            #print v.shape, u.shape, x.shape

            x1 = torch.cat((mask, v, u, x), dim=1)

            x1 = F.leaky_relu(self.nin1(x1))
            x1 = F.leaky_relu(self.nin2(x1))
            x1 = self.nin3(x1)
            return x1

    net = Net()
    return net

def build_optimizer(model):
    import torch.optim

    # optim = torch.optim.SGD(model.parameters(), lr=UPDATER_INIT_LR, momentum=MOMENTUM)
    optim = torch.optim.Adam(model.parameters(), lr=UPDATER_INIT_LR, weight_decay=L2_DECAY)
    return optim


def build_loss():
    from fosforo.objectives.depth import MaskedL1DepthGradLoss2d
    loss = MaskedL1DepthGradLoss2d(1.0, 0.5)
    return loss


def build_callbacks(env):
    from fosforo.monitoring.interval_wrappers import EveryNSeconds
    from fosforo.monitoring.checkpoint_cb import CheckpointCb
    from fosforo.monitoring.tensorboard_cb import TensorboardLogCb

    from mavs6depthproc.monitoring.tensorboard_cb import TensorboardDisparityCb
    from mavs6depthproc.monitoring.tensorboard_cb import TensorboardDispDepthMetricsCb

    # data_base_dir = Path(env['data_base_dir'])
    # class_metadata_fname = data_base_dir/CLASS_METADATA

    preproc = lambda x: (x*1./NORM_SCALE)

    train_cbs = []
    train_cbs.append(EveryNSeconds(CheckpointCb(), interval_sec=SAVE_EVERY_NSEC))
    train_cbs.append(EveryNSeconds(TensorboardLogCb(exp_id+'/train/loss', log_key='loss'), 1))
    train_cbs.append(EveryNSeconds(TensorboardDisparityCb(exp_id+'/train/disp',
                                                          vmin=0.,
                                                          vmax=36.,
                                                          preproc=preproc),
                                    interval_sec=DISPLAY_EVERY_NSEC))
    valid_cbs = [TensorboardDispDepthMetricsCb(exp_id+'/valid', disp_norm_scale=NORM_SCALE, Tx=Tx)]
    return {'train': train_cbs, 'valid': valid_cbs}



def build_datasets(base_dir):
    from mavs6depthproc.data.disparity import DisparityDataset

    import mavs6depthproc.pipelines.disparity1 as disp_pipeline

    train_pipeline = disp_pipeline.build_train_ops(x_key=X_KEY,
                                                   y_key=Y_KEY,
                                                   scale=NORM_SCALE)

    valid_pipeline = disp_pipeline.build_valid_ops(x_key=X_KEY,
                                                   y_key=Y_KEY,
                                                   scale=NORM_SCALE)

    train_ds = DisparityDataset(base_dir/DATA_DIR,
                                base_dir/SPLITS,
                                split_key='train',
                                pipeline=train_pipeline)

    valid_ds = DisparityDataset(base_dir/DATA_DIR,
                                base_dir/SPLITS,
                                split_key='valid',
                                pipeline=valid_pipeline)
    return {'train': train_ds,
            'valid': valid_ds}


if __name__ == "__main__":
    import fosforo.trainer_cli
    fosforo.trainer_cli.SupervisedTrainerAppCli(__file__).cli()
