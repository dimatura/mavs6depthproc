from collections import OrderedDict
from spath import Path

exp_id = str(Path(__file__).basename().stripext())

DATA_DIR = 'disp_dataset/2018_08_23'
SPLITS = DATA_DIR + '/splits1.json'
CLASS_METADATA = ''

DISPX_KEY = 'nerian_disp'
GRAYX_KEY = 'nerian_gray'
X_KEY = 'nerian_graydisp'
Y_KEY = 'colmap_disp'
YHAT_KEY = 'yhat'

TRAIN_BATCH_SIZE = 4
VALID_BATCH_SIZE = 16

VALID_EVERY_NSEC = 5*60
#VALID_EVERY_NSEC = -1
SAVE_EVERY_NSEC = 5*60
DISPLAY_EVERY_NSEC = 2
MAX_EPOCHS = 400000

L2_DECAY = 0.000001
#L2_DECAY = 0.0
#UPDATER_INIT_LR = 0.00001
#UPDATER_INIT_LR = 0.000001
UPDATER_INIT_LR = 0.0000001
#UPDATER_INIT_LR = 1e-9
#UPDATER_INIT_LR = 1e-11
#UPDATER_INIT_LR = 1e-12
#UPDATER_INIT_LR = 5e-13
MOMENTUM = 0.95

NORM_SCALE = 1./16
Tx = -178.443


def build_model(mode='train'):
    import torch
    from torch import nn
    import torch.nn.functional as F

    from fosforo.layers import SparsityInvariantConvRelu2d as SICR
    from fosforo.layers import sparse_sum
    from fosforo.layers import CRP, CBRP, ResConv
    from fosforo.init import normal_dirac_init_
    from fosforo.models.base import FosforoModel

    class ResidualSICR(nn.Module):
        def __init__(self, in_channels, inner_channels, bottleneck_channels, kernel_size):
            super(ResidualSICR, self).__init__()
            self.cbr1 = SICR(in_channels, inner_channels, 3, relu=True)
            self.cbr2 = SICR(inner_channels, in_channels, 3, relu=True)
            if bottleneck_channels > 0:
                self.nin = SICR(in_channels, bottleneck_channels, 1, relu=False)

        def initialize(self):
            normal_dirac_init_(self.cbr1.conv.weight, 0.1)
            normal_dirac_init_(self.cbr2.conv.weight, 0.1)
            nn.init.constant_(self.cbr1.bias, 0.)
            nn.init.constant_(self.cbr2.bias, 0.)
            if hasattr(self, 'nin'):
                nn.init.kaiming_normal_(self.nin.conv.weights)

        def forward(self, mask, x):
            mask1, cbr1 = self.cbr1(mask, x)
            mask2, cbr2 = self.cbr2(mask1, cbr1)
            smask, ssum = sparse_sum(mask, x, mask2, cbr2)
            ssum = F.relu(ssum)
            if hasattr(self, 'nin'):
                smask, ssum = self.nin(smask, ssum)
            return smask, ssum

    class Net(FosforoModel):
        def __init__(self):
            super(Net, self).__init__()
            self.nin0 = nn.Conv2d(1, 1, 1, bias=False)

            self.dcin = SICR(1, 16, 1)
            self.dc1 = SICR(16, 16, 3)
            self.dc2 = SICR(16, 16, 3)
            self.dc3 = SICR(16, 16, 3)

            self.gcin = nn.Conv2d(1, 16, 1, padding=0)
            self.gc1 = nn.Conv2d(16, 16, 3, padding=1)
            self.gc2 = nn.Conv2d(16, 16, 3, padding=1)
            self.gc3 = nn.Conv2d(16, 16, 3, padding=1)

            self.ccat1 = nn.Conv2d(16+16+1, 16, 3, padding=1)
            self.ccat2 = nn.Conv2d(16, 16, 3, padding=1)
            self.dout = nn.Conv2d(16, 1, 1, padding=0)

        def initialize(self):
            nn.init.constant_(self.nin0.weight, 1.)
            noisy_dirac_init_(self.dcin.weight, 0.1)
            noisy_dirac_init_(self.dc1.conv.weight, 0.1)
            noisy_dirac_init_(self.dc2.conv.weight, 0.1)
            noisy_dirac_init_(self.dc3.conv.weight, 0.1)

        def forward(self, x):
            # note we don't use spatial dropout, to add sparsity
            # TODO probably should add a masking op in pipeline

            gray = x[:, 0:1]
            disp = x[:, 1:2]

            disp = F.dropout(disp, p=0.05)
            disp = self.nin0(disp)
            mask = (disp != 0.0).float()

            mask, disp = self.dcin.forward(mask, disp)
            mask, disp = self.dc1.forward(mask, disp)
            mask, disp = self.dc2.forward(mask, disp)
            mask, disp = self.dc3.forward(mask, disp)

            gray = F.leaky_relu(self.gcin.forward(gray))
            gray = F.leaky_relu(self.gc1.forward(gray))
            gray = F.leaky_relu(self.gc2.forward(gray))
            gray = F.leaky_relu(self.gc3.forward(gray))

            dispgraymask = torch.cat((gray, disp, mask), 1)
            ccat1 = F.leaky_relu(self.ccat1(dispgraymask))
            ccat2 = F.leaky_relu(self.ccat2(ccat1))
            dout = self.dout(ccat2)
            # print dout.shape
            return dout

    net = Net()
    return net


def build_optimizer(model):
    import torch.optim

    # optim = torch.optim.SGD(model.parameters(), lr=UPDATER_INIT_LR, momentum=MOMENTUM)
    optim = torch.optim.Adam(model.parameters(), lr=UPDATER_INIT_LR, weight_decay=L2_DECAY)
    return optim


def build_loss():
    from fosforo.objectives.depth import MaskedL1DepthGradLoss2d

    loss = MaskedL1DepthGradLoss2d(1.0, 0.5)
    return loss


def build_callbacks(env):
    from fosforo.monitoring.interval_wrappers import EveryNSeconds
    from fosforo.monitoring.checkpoint_cb import CheckpointCb
    from fosforo.monitoring.tensorboard_cb import TensorboardLogCb

    from mavs6depthproc.monitoring.tensorboard_cb import TensorboardDisparityCb
    from mavs6depthproc.monitoring.tensorboard_cb import TensorboardDispDepthMetricsCb

    # data_base_dir = Path(env['data_base_dir'])
    # class_metadata_fname = data_base_dir/CLASS_METADATA

    preproc = lambda x: (x*1./NORM_SCALE)

    train_cbs = []
    train_cbs.append(EveryNSeconds(CheckpointCb(), interval_sec=SAVE_EVERY_NSEC))
    train_cbs.append(EveryNSeconds(TensorboardLogCb(exp_id+'/train/loss', log_key='loss'), 1))
    train_cbs.append(EveryNSeconds(TensorboardDisparityCb(exp_id+'/train/disp',
                                                          vmin=0.,
                                                          vmax=36.,
                                                          preproc=preproc),
                                    interval_sec=DISPLAY_EVERY_NSEC))
    valid_cbs = [TensorboardDispDepthMetricsCb(exp_id+'/valid', disp_norm_scale=NORM_SCALE, Tx=Tx)]
    return {'train': train_cbs, 'valid': valid_cbs}


def build_datasets(base_dir):
    from mavs6depthproc.data.disparity import RGBDDataset

    import mavs6depthproc.pipelines.rgbd1 as pipeline

    train_pipeline = pipeline.build_train_ops(gray_key=GRAYX_KEY,
                                              disp_key=DISPX_KEY,
                                              x_key=X_KEY,
                                              y_key=Y_KEY,
                                              scale=NORM_SCALE)

    valid_pipeline = pipeline.build_valid_ops(gray_key=GRAYX_KEY,
                                              disp_key=DISPX_KEY,
                                              x_key=X_KEY,
                                              y_key=Y_KEY,
                                              scale=NORM_SCALE)

    train_ds = RGBDDataset(base_dir/DATA_DIR,
                           base_dir/SPLITS,
                           split_key='train',
                           pipeline=train_pipeline)

    valid_ds = RGBDDataset(base_dir/DATA_DIR,
                           base_dir/SPLITS,
                           split_key='valid',
                           pipeline=valid_pipeline)
    return {'train': train_ds,
            'valid': valid_ds}


if __name__ == "__main__":
    import fosforo.trainer_cli
    fosforo.trainer_cli.SupervisedTrainerAppCli(__file__).cli()
