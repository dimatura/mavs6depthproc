from collections import OrderedDict
from spath import Path

exp_id = str(Path(__file__).basename().stripext())

# DATA_DIR = '2018_08_23'
DATA_DIR = 'disp_dataset/2018_08_23'
SPLITS = DATA_DIR + '/splits1.json'
CLASS_METADATA = ''

X_KEY = 'nerian_disp'
Y_KEY = 'colmap_disp'
YHAT_KEY = 'yhat'

TRAIN_BATCH_SIZE = 4
VALID_BATCH_SIZE = 16

#VALID_EVERY_NSEC = 30*60
VALID_EVERY_NSEC = -1
SAVE_EVERY_NSEC = 5*60
DISPLAY_EVERY_NSEC = 2
MAX_EPOCHS = 4000

#L2_DECAY = 0.00001
L2_DECAY = 0.000001
UPDATER_INIT_LR = 0.00001
#UPDATER_INIT_LR = 0.000001
#UPDATER_INIT_LR = 0.0000001
#UPDATER_INIT_LR = 1e-9
#UPDATER_INIT_LR = 1e-11
#UPDATER_INIT_LR = 1e-12
#UPDATER_INIT_LR = 5e-13
MOMENTUM = 0.99

NORM_SCALE = 1./16


def build_model(mode='train'):
    import torch
    from torch import nn
    import torch.nn.functional as F

    from fosforo.layers import SparsityInvariantConvRelu2d as SICR
    from fosforo.layers import sparse_sum
    from fosforo.layers import CRP, CBRP, ResConv
    from fosforo.init import normal_dirac_init_
    from fosforo.models.base import FosforoModel

    class SoftMaskConv(nn.Module):
        def __init__(self, in_channels, mask_in_channels, out_channels, kernel_size):
            super(SoftMaskConv, self).__init__()
            padding = int(kernel_size/2)
            self.scr = nn.Conv2d(in_channels, out_channels, kernel_size, padding=padding)
            self.mcr = nn.Conv2d(mask_in_channels, out_channels, kernel_size, padding=padding)

        def initialize(self):
            normal_dirac_init_(self.scr.weight, 0.1)
            #nn.init.constant_(self.mcr.weight, 1.)
            normal_dirac_init_(self.mcr.weight, 0.1)

        def forward(self, mask, x):
            mcr = torch.sigmoid(self.mcr(mask))
            scr = F.leaky_relu(self.scr(x))
            out = mcr*scr
            return mcr, out

    class Net(FosforoModel):
        def __init__(self):
            super(Net, self).__init__()
            self.nin0 = nn.Conv2d(1, 1, 1, bias=False)
            self.smc1 = SoftMaskConv(1, 1, 16, 3)
            self.smc2 = SoftMaskConv(16, 16, 16, 3)
            self.smc3 = SoftMaskConv(16, 16, 16, 3)
            self.smc4 = SoftMaskConv(16, 16, 16, 3)
            self.smcout = SoftMaskConv(16, 16, 1, 1)

        def initialize(self):
            nn.init.constant_(self.nin0.weight, 1.)
            self.smc1.initialize()
            self.smc2.initialize()
            self.smc3.initialize()
            self.smc4.initialize()
            self.smcout.initialize()

        def forward(self, x):
            # note we don't use spatial dropout, to add sparsity
            # TODO probably should add a masking op in pipeline
            x = F.dropout(x, p=0.05)
            mask = (x != 0.0).float()

            x = self.nin0(x)

            mask, x = self.smc1(mask, x)
            mask, x = self.smc2(mask, x)
            mask, x = self.smc3(mask, x)
            mask, x = self.smc4(mask, x)
            mask, x = self.smcout(mask, x)

            return x

    net = Net()
    return net


def build_optimizer(model):
    import torch.optim

    # optim = torch.optim.SGD(model.parameters(), lr=UPDATER_INIT_LR, momentum=MOMENTUM)
    optim = torch.optim.Adam(model.parameters(), lr=UPDATER_INIT_LR, weight_decay=L2_DECAY)
    return optim


def build_loss():
    from fosforo.objectives.depth import MaskedL1DepthGradLoss2d
    from fosforo.objectives.depth import MaskedL1DispAsDepthLoss2d
    Tx = -178.44
    loss = MaskedL1DepthGradLoss2d(1.0, 0.5, size_average=True)
    #loss = MaskedL1DispAsDepthLoss2d(Tx)
    return loss


def build_callbacks(env):
    from fosforo.monitoring.interval_wrappers import EveryNSeconds
    from fosforo.monitoring.checkpoint_cb import CheckpointCb
    from fosforo.monitoring.tensorboard_cb import TensorboardLogCb

    from mavs6depthproc.monitoring.tensorboard_cb import TensorboardDisparityCb

    # data_base_dir = Path(env['data_base_dir'])
    # class_metadata_fname = data_base_dir/CLASS_METADATA

    preproc = lambda x: (x*1./NORM_SCALE)

    train_cbs = []
    train_cbs.append(EveryNSeconds(CheckpointCb(), interval_sec=SAVE_EVERY_NSEC))
    train_cbs.append(EveryNSeconds(TensorboardLogCb(exp_id+'_train_loss', 'loss'), 1))
    train_cbs.append(EveryNSeconds(TensorboardDisparityCb(exp_id+'_disp',
                                                          vmin=0.,
                                                          vmax=36.,
                                                          preproc=preproc),
                                    interval_sec=DISPLAY_EVERY_NSEC))
    valid_cbs = []
    return {'train': train_cbs, 'valid': valid_cbs}


def build_datasets(base_dir):
    from mavs6depthproc.data.disparity import DisparityDataset

    import mavs6depthproc.pipelines.disparity1 as disp_pipeline

    train_pipeline = disp_pipeline.build_train_ops(x_key=X_KEY,
                                                   y_key=Y_KEY,
                                                   scale=NORM_SCALE)

    valid_pipeline = disp_pipeline.build_valid_ops(x_key=X_KEY,
                                                   y_key=Y_KEY,
                                                   scale=NORM_SCALE)

    train_ds = DisparityDataset(base_dir/DATA_DIR,
                                base_dir/SPLITS,
                                split_key='train',
                                x_fname='nerian_disp8.png',
                                y_fname='colmap_disp8.png',
                                pipeline=train_pipeline)

    valid_ds = DisparityDataset(base_dir/DATA_DIR,
                                base_dir/SPLITS,
                                split_key='valid',
                                x_fname='nerian_disp8.png',
                                y_fname='colmap_disp8.png',
                                pipeline=valid_pipeline)


    return {'train': train_ds,
            'valid': valid_ds}

if __name__ == "__main__":
    import fosforo.trainer_cli
    fosforo.trainer_cli.SupervisedTrainerAppCli(__file__).cli()
