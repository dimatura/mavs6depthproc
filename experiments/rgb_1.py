from collections import OrderedDict
from spath import Path

exp_id = str(Path(__file__).basename().stripext())

DATA_DIR = 'disp_dataset/2018_08_23'
SPLITS = DATA_DIR + '/splits1.json'
CLASS_METADATA = ''

DISPX_KEY = 'nerian_disp'
GRAYX_KEY = 'nerian_gray'
X_KEY = 'nerian_graydisp'
Y_KEY = 'colmap_disp'
YHAT_KEY = 'yhat'

TRAIN_BATCH_SIZE = 4
VALID_BATCH_SIZE = 16

VALID_EVERY_NSEC = 5*60
#VALID_EVERY_NSEC = -1
SAVE_EVERY_NSEC = 5*60
DISPLAY_EVERY_NSEC = 2
MAX_EPOCHS = 400000

L2_DECAY = 0.000001
#L2_DECAY = 0.0
UPDATER_INIT_LR = 0.00001
#UPDATER_INIT_LR = 0.000001
#UPDATER_INIT_LR = 0.0000001
#UPDATER_INIT_LR = 1e-9
#UPDATER_INIT_LR = 1e-11
#UPDATER_INIT_LR = 1e-12
#UPDATER_INIT_LR = 5e-13
MOMENTUM = 0.95

NORM_SCALE = 1./16
Tx = -178.443


def build_model(mode='train'):
    import torch
    from torch import nn
    import torch.nn.functional as F

    # from fosforo.layers import SparsityInvariantConvRelu2d as SICR
    # from fosforo.layers import sparse_sum
    from fosforo.layers import CRP, CBRP, ResConv
    from fosforo.init import normal_dirac_init_
    from fosforo.models.base import FosforoModel

    class DownBlock(nn.Module):
        def __init__(self, in_channels, out_channels1, out_channels2, kernel_size=3):
            super(DownBlock, self).__init__()
            padding = int(kernel_size//2)
            self.conv = nn.Conv2d(in_channels, out_channels1, kernel_size, padding=padding)
            self.relu = nn.LeakyReLU()
            self.pool = nn.MaxPool2d(2, 2)
            self.nin = nn.Conv2d(out_channels1, out_channels2, 1)
            self.nin_relu = nn.LeakyReLU()

        def forward(self, x):
            x = self.relu(self.conv(x))
            return self.pool(x), self.nin_relu(self.nin(x))


    class UpBlock(nn.Module):
        def __init__(self, in_channels, out_channels, kernel_size=1):
            super(UpBlock, self).__init__()
            padding = int(kernel_size//2)
            self.up = nn.Upsample(mode='bilinear', align_corners=True, scale_factor=2)
            self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, padding=padding)

        def forward(self, x1, x2):
            up = self.up(x1) + x2
            out = self.conv(up)
            return out

    class Net(FosforoModel):
        def __init__(self):
            super(Net, self).__init__()
            # 64, 80
            # 32, 40
            # 16, 20
            #  8, 10

            self.down1 = DownBlock(1, 16, 64) # 32, 40
            self.down2 = DownBlock(16, 32, 64) # 16, 20
            self.down3 = DownBlock(32, 64, 64) # 8, 10
            self.down4 = DownBlock(64, 64, 64) # 4, 5

            self.nin5 = nn.Conv2d(64, 64, 1)

            self.up1 = UpBlock(64, 64) # 8, 10
            self.up2 = UpBlock(64, 64) # 16, 20
            self.up3 = UpBlock(64, 64) # 32, 40
            self.up4 = UpBlock(64, 64) # 64, 80

            self.cout = nn.Conv2d(64, 1, 1)

        def initialize(self):
            #nn.init.constant_(self.nin0.weight, 1.)
            #noisy_dirac_init_(self.dcin.weight, 0.1)
            #noisy_dirac_init_(self.dc1.conv.weight, 0.1)
            #noisy_dirac_init_(self.dc2.conv.weight, 0.1)
            #noisy_dirac_init_(self.dc3.conv.weight, 0.1)
            pass

        def forward(self, x):
            # get grayscale img
            gx = x[:, 0:1]
            #disp = x[:, 1:2]
            #disp = F.dropout(disp, p=0.05)

            # pad to 64, 80. top bottom left right
            gx = F.pad(gx, (0, 0, 2, 2))

            d1down, d1 = self.down1(gx)
            d2down, d2 = self.down2(d1down)
            d3down, d3 = self.down3(d2down)
            d4down, d4 = self.down4(d3down)

            nin = F.leaky_relu(self.nin5(d4down))

            u1 = self.up1(nin, d4)
            u2 = self.up2(u1, d3)
            u3 = self.up3(u2, d2)
            u4 = self.up4(u3, d1)

            cout = self.cout(u4)
            # unpad
            cout = cout[:, :, 2:2+x.size(2), :]
            return cout

    net = Net()
    return net


def build_optimizer(model):
    import torch.optim

    # optim = torch.optim.SGD(model.parameters(), lr=UPDATER_INIT_LR, momentum=MOMENTUM)
    optim = torch.optim.Adam(model.parameters(), lr=UPDATER_INIT_LR, weight_decay=L2_DECAY)
    return optim


def build_loss():
    from fosforo.objectives.depth import MaskedL1DepthGradLoss2d

    loss = MaskedL1DepthGradLoss2d(1.0, 0.5)
    return loss


def build_callbacks(env):
    from fosforo.monitoring.interval_wrappers import EveryNSeconds
    from fosforo.monitoring.checkpoint_cb import CheckpointCb
    from fosforo.monitoring.tensorboard_cb import TensorboardLogCb

    from mavs6depthproc.monitoring.tensorboard_cb import TensorboardDisparityCb
    from mavs6depthproc.monitoring.tensorboard_cb import TensorboardDispDepthMetricsCb

    # data_base_dir = Path(env['data_base_dir'])
    # class_metadata_fname = data_base_dir/CLASS_METADATA

    preproc = lambda x: (x*1./NORM_SCALE)

    train_cbs = []
    train_cbs.append(EveryNSeconds(CheckpointCb(), interval_sec=SAVE_EVERY_NSEC))
    train_cbs.append(EveryNSeconds(TensorboardLogCb(exp_id+'/train/loss', log_key='loss'), 1))
    train_cbs.append(EveryNSeconds(TensorboardDisparityCb(exp_id+'/train/disp',
                                                          vmin=0.,
                                                          vmax=36.,
                                                          preproc=preproc),
                                    interval_sec=DISPLAY_EVERY_NSEC))
    valid_cbs = [TensorboardDispDepthMetricsCb(exp_id+'/valid', disp_norm_scale=NORM_SCALE, Tx=Tx)]
    return {'train': train_cbs, 'valid': valid_cbs}



def build_datasets(base_dir):
    from mavs6depthproc.data.disparity import RGBDDataset

    import mavs6depthproc.pipelines.rgbd1 as pipeline

    train_pipeline = pipeline.build_train_ops(gray_key=GRAYX_KEY,
                                              disp_key=DISPX_KEY,
                                              x_key=X_KEY,
                                              y_key=Y_KEY,
                                              scale=NORM_SCALE)

    valid_pipeline = pipeline.build_valid_ops(gray_key=GRAYX_KEY,
                                              disp_key=DISPX_KEY,
                                              x_key=X_KEY,
                                              y_key=Y_KEY,
                                              scale=NORM_SCALE)

    train_ds = RGBDDataset(base_dir/DATA_DIR,
                           base_dir/SPLITS,
                           split_key='train',
                           dispx_fname='nerian_disp8.png',
                           grayx_fname='rgb8.jpg',
                           y_fname='colmap_disp8.png',
                           pipeline=train_pipeline)

    valid_ds = RGBDDataset(base_dir/DATA_DIR,
                           base_dir/SPLITS,
                           dispx_fname='nerian_disp8.png',
                           grayx_fname='rgb8.jpg',
                           y_fname='colmap_disp8.png',
                           split_key='valid',
                           pipeline=valid_pipeline)
    return {'train': train_ds,
            'valid': valid_ds}


if __name__ == "__main__":
    import fosforo.trainer_cli
    fosforo.trainer_cli.SupervisedTrainerAppCli(__file__).cli()
