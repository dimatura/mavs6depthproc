from collections import OrderedDict
from spath import Path

exp_id = str(Path(__file__).basename().stripext())

# DATA_DIR = '2018_08_23'
DATA_DIR = 'disp_dataset/2018_08_23'
SPLITS = DATA_DIR + '/splits1.json'
CLASS_METADATA = ''

X_KEY = 'nerian_disp'
Y_KEY = 'colmap_disp'
YHAT_KEY = 'yhat'

TRAIN_BATCH_SIZE = 4
VALID_BATCH_SIZE = 16

#VALID_EVERY_NSEC = 30*60
VALID_EVERY_NSEC = -1
SAVE_EVERY_NSEC = 5*60
DISPLAY_EVERY_NSEC = 2
MAX_EPOCHS = 4000

#L2_DECAY = 0.00001
L2_DECAY = 0.0
#UPDATER_INIT_LR = 0.00001
#UPDATER_INIT_LR = 0.000001
UPDATER_INIT_LR = 0.0000001
#UPDATER_INIT_LR = 1e-9
#UPDATER_INIT_LR = 1e-11
#UPDATER_INIT_LR = 1e-12
#UPDATER_INIT_LR = 5e-13
MOMENTUM = 0.99

NORM_SCALE = 1./16


def build_model(mode='train'):
    import torch
    from torch import nn
    import torch.nn.functional as F

    from fosforo.layers import SparsityInvariantConvRelu2d as SICR
    from fosforo.layers import sparse_sum
    from fosforo.layers import CRP, CBRP, ResConv
    from fosforo.init import normal_dirac_init_
    from fosforo.models.base import FosforoModel

    class ResidualSICR(nn.Module):
        def __init__(self, in_channels, inner_channels, bottleneck_channels, kernel_size):
            super(ResidualSICR, self).__init__()
            self.cbr1 = SICR(in_channels, inner_channels, 3, relu=True)
            self.cbr2 = SICR(inner_channels, in_channels, 3, relu=True)
            if bottleneck_channels > 0:
                self.nin = SICR(in_channels, bottleneck_channels, 1, relu=False)

        def initialize(self):
            normal_dirac_init_(self.cbr1.conv.weight, 0.1)
            normal_dirac_init_(self.cbr2.conv.weight, 0.1)
            nn.init.constant_(self.cbr1.bias, 0.)
            nn.init.constant_(self.cbr2.bias, 0.)
            if hasattr(self, 'nin'):
                nn.init.kaiming_normal_(self.nin.conv.weights)

        def forward(self, mask, x):
            mask1, cbr1 = self.cbr1(mask, x)
            mask2, cbr2 = self.cbr2(mask1, cbr1)
            smask, ssum = sparse_sum(mask, x, mask2, cbr2)
            ssum = F.relu(ssum)
            if hasattr(self, 'nin'):
                smask, ssum = self.nin(smask, ssum)
            return smask, ssum

    class Net(FosforoModel):
        def __init__(self):
            super(Net, self).__init__()
            self.nin0 = nn.Conv2d(1, 1, 1, bias=False)
            self.cbrin = SICR(1, 16, 1)
            self.rcbr1 = ResidualSICR(16, 16, 0, 3)
            self.rcbr2 = ResidualSICR(16, 16, 0, 3)
            self.rcbr3 = ResidualSICR(16, 16, 0, 3)
            self.cbrout = SICR(16, 1, 1, relu=False)

            self.nincat = nn.Conv2d(4, 16, 1)

            self.drc1 = ResConv(16, 16, 3)
            self.drc2 = ResConv(16, 16, 3)
            self.drc3 = ResConv(16, 16, 3)
            self.dout = nn.Conv2d(16, 1, 1)


        def initialize(self):
            nn.init.constant_(self.nin0.weight, 1.)
            normal_dirac_init_(self.cbrin.conv.weight, 0.1)
            self.rcbr1.initialize()
            self.rcbr2.initialize()
            self.rcbr3.initialize()

            self.drc1.initialize(normal_dirac_init_)
            self.drc2.initialize(normal_dirac_init_)
            self.drc3.initialize(normal_dirac_init_)
            nn.init.kaiming_normal_(self.cbrout.conv.weight)

        def sparse_forward(self, mask, x):
            mask, x = self.cbrin(mask, x)
            mask, x = self.rcbr1(mask, x)
            mask, x = self.rcbr2(mask, x)
            mask, x = self.rcbr3(mask, x)
            mask, x = self.cbrout(mask, x)
            return mask, x

        def forward(self, x):
            # note we don't use spatial dropout, to add sparsity
            # TODO probably should add a masking op in pipeline
            x0 = F.dropout(x, p=0.05)
            x0 = self.nin0(x0)

            mask0 = (x0 != 0.0).float()

            mask1, x1 = self.sparse_forward(mask0, x0)

            xcat = torch.cat((mask0, mask1, x0, x1), dim=1)
            xcat = self.nincat(xcat)
            xcat = self.drc1(xcat)
            xcat = self.drc2(xcat)
            xcat = self.drc3(xcat)
            out = self.dout(xcat)
            return out

    net = Net()
    return net


def build_optimizer(model):
    import torch.optim

    # optim = torch.optim.SGD(model.parameters(), lr=UPDATER_INIT_LR, momentum=MOMENTUM)
    optim = torch.optim.Adam(model.parameters(), lr=UPDATER_INIT_LR, weight_decay=L2_DECAY)
    return optim


def build_loss():
    from fosforo.objectives.depth import MaskedL1DepthGradLoss2d
    from fosforo.objectives.depth import MaskedL1DispAsDepthLoss2d
    Tx = -178.44
    #loss = MaskedL1DepthGradLoss2d(1.0, 0.5)
    loss = MaskedL1DispAsDepthLoss2d(Tx)
    return loss


def build_callbacks(env):
    from fosforo.monitoring.interval_wrappers import EveryNSeconds
    from fosforo.monitoring.checkpoint_cb import CheckpointCb
    from fosforo.monitoring.tensorboard_cb import TensorboardLogCb

    from mavs6depthproc.monitoring.tensorboard_cb import TensorboardDisparityCb

    # data_base_dir = Path(env['data_base_dir'])
    # class_metadata_fname = data_base_dir/CLASS_METADATA

    preproc = lambda x: (x*1./NORM_SCALE)

    train_cbs = []
    train_cbs.append(EveryNSeconds(CheckpointCb(), interval_sec=SAVE_EVERY_NSEC))
    train_cbs.append(EveryNSeconds(TensorboardLogCb(exp_id+'_train_loss', 'loss'), 1))
    train_cbs.append(EveryNSeconds(TensorboardDisparityCb(exp_id+'_disp',
                                                          vmin=0.,
                                                          vmax=36.,
                                                          preproc=preproc),
                                    interval_sec=DISPLAY_EVERY_NSEC))
    valid_cbs = []
    return {'train': train_cbs, 'valid': valid_cbs}


def build_datasets(base_dir):
    from mavs6depthproc.data.disparity import DisparityDataset

    import mavs6depthproc.pipelines.disparity1 as disp_pipeline

    train_pipeline = disp_pipeline.build_train_ops(x_key=X_KEY,
                                                   y_key=Y_KEY,
                                                   scale=NORM_SCALE)

    valid_pipeline = disp_pipeline.build_valid_ops(x_key=X_KEY,
                                                   y_key=Y_KEY,
                                                   scale=NORM_SCALE)

    train_ds = DisparityDataset(base_dir/DATA_DIR,
                                base_dir/SPLITS,
                                split_key='train',
                                pipeline=train_pipeline)

    valid_ds = DisparityDataset(base_dir/DATA_DIR,
                                base_dir/SPLITS,
                                split_key='valid',
                                pipeline=valid_pipeline)
    return {'train': train_ds,
            'valid': valid_ds}

if __name__ == "__main__":
    import fosforo.trainer_cli
    fosforo.trainer_cli.SupervisedTrainerAppCli(__file__).cli()
