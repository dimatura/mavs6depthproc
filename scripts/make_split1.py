from spath import Path
import random
import hashlib

base_dir = Path('/mnt/guataca/easystore04/mavs6_ws/disp_dataset/2018_08_23')

dirs = base_dir.dirs()

split = {'train': [], 'valid': []}

for d in dirs:
    iid = d.basename()
    md5 = hashlib.md5()
    md5.update(str(iid))
    h = int(md5.hexdigest(), 16)
    if h % 100 < 80:
        split['train'].append(d.relpath(base_dir))
    else:
        split['valid'].append(d.relpath(base_dir))

print(len(split['train']), len(split['valid']))
outfname = base_dir/'splits1.json'
outfname.write_json(split, indent=2)
